import {forEach, values, merge} from 'lodash/fp';

import sendMessagePlugin from './send-message';

const plugins = {
  telegram_send_message: sendMessagePlugin,
};

forEach(p => {
  p.argv = merge({  // eslint-disable-line no-param-reassign
    'telegram.bot_key': {
      type: 'string',
      desc: 'An API key for your bot authentication.',
    },
  }, p.argv);
}, values(plugins));

export { plugins };
export default { plugins };
